# BDD tests on HMRC dummy site

Project to demostrate BDD capabilities on hmrc website

## Requirement

Requirements are specified in attached PDF file

##Tools
This framework requires following tools
 * java 1.8 
 * Maven 3 
 * Latest chrome browser 
 * IntelliJ (only for enhancing framework / debugging) 

## Execute tests
Run the following command from command prompt pointing to this project
directory
```
mvn clean test 
```
