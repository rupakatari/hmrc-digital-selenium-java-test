Feature: User successfully creates an account

  Scenario: User successfully creates account - Happy path
    Given User is on index page
    And User clicks signIn link
    And User enters an 'random' on email field
    And User clicks on Create an account button
    And User is shown 'Login - My Store' page
    And User enters following personal data
      | First name | Asdf        |
      | Last name  | Qwerty      |
      | Password   | Password123 |
    And User enters following address data
      | Address                 | London Road   |
      | City                    | myCity        |
      | State                   | Alabama       |
      | Zip/Postal Code         | 12345         |
      | Country                 | United States |
      | Mobile phone            | 079900000000  |
      | Assign an address alias | My Address1   |
    When User clicks Register button
    Then User is shown Sucess page

    