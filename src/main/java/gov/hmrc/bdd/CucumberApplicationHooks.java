package gov.hmrc.bdd;

import com.google.inject.Inject;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import static gov.hmrc.util.MiscUtil.isWindowsOS;


public class CucumberApplicationHooks {

    private static String driverName = "/chromedriver";

     static {
        if (isWindowsOS()) {
            driverName = driverName + ".exe";
        }
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + driverName);
    }

    private final int TIME_WAIT = 10000;

    @Inject
    ScenarioContainer scenarioContainer;

    @Before
    public void beforeEveryTest() throws Exception {
        scenarioContainer.webDriver = new ChromeDriver();
    }

        @After
        public void afterEveryTest () {

            try {
                Thread.sleep(TIME_WAIT);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //scenarioContainer.webDriver.quit();
        }

    }