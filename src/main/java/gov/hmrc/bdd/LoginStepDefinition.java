package gov.hmrc.bdd;

import static gov.hmrc.util.AppConstants.ADDITIONAL_INFO;
import static gov.hmrc.util.AppConstants.ADDRESS;
import static gov.hmrc.util.AppConstants.ADDRESS2;
import static gov.hmrc.util.AppConstants.ADDRESS_ALIAS;
import static gov.hmrc.util.AppConstants.CITY;
import static gov.hmrc.util.AppConstants.COUNTRY;
import static gov.hmrc.util.AppConstants.FIRST_NAME;
import static gov.hmrc.util.AppConstants.LAST_NAME;
import static gov.hmrc.util.AppConstants.MOBILE_PHONE;
import static gov.hmrc.util.AppConstants.PASSWORD;
import static gov.hmrc.util.AppConstants.STATE;
import static gov.hmrc.util.AppConstants.ZIP_POSTAL_CODE;

import java.util.Map;
import java.util.Random;

import org.openqa.selenium.WebDriver;

import com.google.inject.Inject;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import gov.hmrc.pages.CreateAccountPage;
import gov.hmrc.pages.IndexPage;
import gov.hmrc.pages.SignUpSignIn;

@ScenarioScoped
public class LoginStepDefinition {

    private IndexPage indexPage;
    private SignUpSignIn signUpSignIn;
    private WebDriver driver;
    private CreateAccountPage createAccountPage;

    //load from properties file
    protected String BASE_URL = "http://automationpractice.com/index.php";

    @Inject
    public LoginStepDefinition(ScenarioContainer scenarioContainer) {

        driver = scenarioContainer.webDriver;

        indexPage = new IndexPage(driver);
        signUpSignIn = new SignUpSignIn(driver);
        createAccountPage = new CreateAccountPage(driver);
    }

    @Given("^User is on index page$")
    public void user_is_on_index_page() {
        indexPage.navigateTo(BASE_URL);
    }

    @Given("^User clicks signIn link$")
    public void user_clicks_signin_link() {
        indexPage.clickSignIn();
    }

    @Given("^User enters an \'([^\"]*)\' on email field$")
    public void user_enters_an_on_email_field(String email) throws Throwable {
        if ("random".equals(email)) {
            Random random = new Random();
            int number = random.nextInt(1000) + 1;
            email = "testmail" + number + "@gmail.com";
        }
        System.out.println("email " + email);
        signUpSignIn.enterEmailId(email);
    }

    @Given("^User clicks on Create an account button$")
    public void user_clicks_on_create_an_account_button() throws Throwable {
        signUpSignIn.clickCreateAnAccount();
    }

    @Given("^User is shown \'([^\"]*)\' page$")
    public void user_is_shown__page(String pageTitle) throws Throwable {
//        boolean isEquals = StringUtils.equals(pageTitle,signUpSignIn.getPageTitle());
//        throwRuntimeException(isEquals, "unexpected page");
        signUpSignIn.waitForPageTitle(pageTitle);

    }

    @Given("^User enters following personal data$")
    public void user_enters_following_personal_data(DataTable dataTable) throws Throwable {
        Map<String, String> mapData = dataTable.asMap(String.class, String.class);
        System.out.println(mapData);
        String firstName = mapData.get(FIRST_NAME);
        String lastName = mapData.get(LAST_NAME);
        String password = mapData.get(PASSWORD);
        System.out.println(mapData);

        createAccountPage.waitForPageToLoad();

        if (firstName != null) {
            createAccountPage.enterFirstName(firstName);
        }
        if (lastName != null) {
            createAccountPage.enterLastName(lastName);
        }
        if (password != null) {
            createAccountPage.enterPassword(password);
        }

    }

    @Given("^User enters following address data$")
    public void user_enters_following_address_data(DataTable dataTable) throws Throwable {
        Map<String, String> mapData = dataTable.asMap(String.class, String.class);
        System.out.println(mapData);
        String address1 = mapData.get(ADDRESS);
        String address2 = mapData.get(ADDRESS2);
        String city = mapData.get(CITY);
        String state = mapData.get(STATE);
        String zipcode = mapData.get(ZIP_POSTAL_CODE);
        String country = mapData.get(COUNTRY);
        String additionalInfo = mapData.get(ADDITIONAL_INFO);
        String mobilePhone = mapData.get(MOBILE_PHONE);
        String addressAlias = mapData.get(ADDRESS_ALIAS);
        if (address1 != null) {
            createAccountPage.enterAddress1(address1);
        }

        if (city != null) {
            createAccountPage.enterCity(city);
        }

        if (state != null) {
            createAccountPage.chooseStateDropDown(state);
        }

        if (zipcode != null) {
            createAccountPage.enterZipcode(zipcode);
        }

        if (country != null) {
            createAccountPage.chooseCountryDropDown(country);
        }
        if (mobilePhone != null) {
            createAccountPage.enterMobilePhone(mobilePhone);
        }
        if (addressAlias != null) {
            createAccountPage.enterAddressAlias(addressAlias);
        }

    }

    @When("^User clicks Register button$")
    public void user_clicks_register_button() throws Throwable {
        createAccountPage.register();
    }

    @Then("^User is shown Sucess page$")
    public void user_is_shown_sucsess_page() throws Throwable {
        //TODO
    }
}
