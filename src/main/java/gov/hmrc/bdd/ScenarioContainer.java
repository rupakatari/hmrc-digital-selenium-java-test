package gov.hmrc.bdd;

import cucumber.runtime.java.guice.ScenarioScoped;
import org.openqa.selenium.WebDriver;

@ScenarioScoped
public class ScenarioContainer {
    protected WebDriver webDriver;
}
