package gov.hmrc.util;

public class MiscUtil {

    public static boolean  isWindowsOS(){
        return (System.getProperty("os.name").toLowerCase().indexOf("win") > -1);
    }

    public static void throwRuntimeException(boolean isThrowExceprtion, String message){
        if(isThrowExceprtion){
            throw new RuntimeException(message);
        }
    }
}
