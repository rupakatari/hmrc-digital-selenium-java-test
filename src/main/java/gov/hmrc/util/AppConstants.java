package gov.hmrc.util;

public interface AppConstants {
    String FIRST_NAME = "First name";
    String LAST_NAME = "Last name";
    String PASSWORD = "Password";
    String ADDRESS = "Address";
    String ADDRESS2 = "Address (Line 2)";
    String CITY = "City";
    String STATE = "State";
    String ZIP_POSTAL_CODE = "Zip/Postal Code";
    String COUNTRY = "Country";
    String ADDITIONAL_INFO = "Additional information";
    String MOBILE_PHONE = "Mobile phone";
    String ADDRESS_ALIAS = "Assign an address alias";
}
