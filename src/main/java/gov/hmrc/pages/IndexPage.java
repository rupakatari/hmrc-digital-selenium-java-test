package gov.hmrc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class IndexPage extends BasePage {

    @FindBy(linkText = "Sign in")
    WebElement signInLink;

    public IndexPage(WebDriver driver) {
        super(driver);
    }

    public void clickSignIn(){
        waitForElementToBeClickable(signInLink);
        signInLink.click();
    }

}
