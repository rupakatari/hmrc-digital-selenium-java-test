package gov.hmrc.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {

    protected WebDriver webDriver;

    //load from properties file
    protected static final int WAIT_TIME = 10;
    private WebDriverWait webDriverWait;

    public BasePage(WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(driver, this);
    }

   protected void waitForElementToBeClickable(WebElement webElement) {
        webDriverWait = new WebDriverWait(webDriver, WAIT_TIME);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    protected void waitForVisibilityOf(WebElement webElement) {
        webElement.clear();
        webDriverWait = new WebDriverWait(webDriver, WAIT_TIME);
        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
    }

    protected void waitForElementToBeSelected(WebElement webElement) {
        webElement.clear();
        webDriverWait = new WebDriverWait(webDriver, WAIT_TIME);
        webDriverWait.until(ExpectedConditions.elementToBeSelected(webElement));
    }

    public void navigateTo(String url) {
        String currentWindowHandler = webDriver.getWindowHandle();
        webDriver.switchTo().window(currentWindowHandler);
        webDriver.navigate().to(url);

        webDriver.manage().window().maximize();
    }

    public void waitForPageTitle(String pageTitle){
        WebDriverWait wait = new WebDriverWait(webDriver, WAIT_TIME);
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }

    public void waitForPageToLoadCompletely(){
        new WebDriverWait(webDriver, WAIT_TIME).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }
}
