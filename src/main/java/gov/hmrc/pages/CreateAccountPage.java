package gov.hmrc.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateAccountPage extends BasePage {


    @FindBy(name = "customer_firstname")
    WebElement personalFirstNameField;

    @FindBy(id = "customer_lastname")
    WebElement personalLastNameField;

    @FindBy(id = "passwd")
    WebElement passwordField;

    @FindBy(id = "address1")
    WebElement address1Field;

    @FindBy(id = "city")
    WebElement cityField;

    @FindBy(id = "id_state")
    WebElement stateDropDown;

    @FindBy(id = "postcode")
    WebElement zip_postCodeField;

    @FindBy(id = "id_country")
    WebElement countryDropDown;

    @FindBy(id = "phone_mobile")
    WebElement mobilePhoneField;

    @FindBy(id = "alias")
    WebElement addressAliasField;

    @FindBy(id = "submitAccount")
    WebElement registerButton;

    private static String PAGE_HEADING = "CREATE AN ACCOUNT";

    public CreateAccountPage(WebDriver driver) {
        super(driver);
    }

    public void waitForPageToLoad() {
        try {
            Thread.sleep(8 * WAIT_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        waitForPageToLoadCompletely();
        WebElement pageHeading = webDriver.findElement(By.xpath("//h1[@class='page-heading']"));
//        WebDriverWait wait = new WebDriverWait(webDriver, 3 * WAIT_TIME);
//        wait.until(ExpectedConditions.textToBePresentInElement(pageHeading, PAGE_HEADING));
        String headingText = pageHeading.getText();
        System.out.println(headingText);
        if (!PAGE_HEADING.equals(headingText)) {
            throw new NoSuchElementException("No such element with heading " + headingText);
        }
    }

    public void enterFirstName(String firstName) {

        waitForVisibilityOf(webDriver.findElement(By.name("customer_firstname")));
        personalFirstNameField.sendKeys(firstName);

    }

    public void enterLastName(String lastName) {
        waitForVisibilityOf(personalLastNameField);
        personalLastNameField.sendKeys(lastName);
    }

    public void enterPassword(String password) {
        waitForVisibilityOf(passwordField);
        passwordField.sendKeys(password);
    }

    public void enterAddress1(String address) {
        address1Field.sendKeys(address);
    }

    public void enterCity(String city) {
        waitForVisibilityOf(cityField);
        cityField.sendKeys(city);
    }

    public void chooseStateDropDown(String state) {
        Select stateSelect = new Select(stateDropDown);
        stateSelect.selectByVisibleText(state);
    }

    public void enterZipcode(String postcode) {
        waitForVisibilityOf(zip_postCodeField);
        zip_postCodeField.sendKeys(postcode);
    }

    public void chooseCountryDropDown(String country) {
        Select countrySelect = new Select(countryDropDown);
        countrySelect.selectByVisibleText(country);
    }

    public void enterMobilePhone(String mobileNo) {
        waitForVisibilityOf(mobilePhoneField);
        mobilePhoneField.sendKeys(mobileNo);
    }

    public void enterAddressAlias(String alias) {
        waitForVisibilityOf(addressAliasField);
        addressAliasField.sendKeys(alias);
    }

    public void register(){
        waitForElementToBeClickable(registerButton);
        registerButton.click();
    }
}
