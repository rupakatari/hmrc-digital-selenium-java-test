package gov.hmrc.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignUpSignIn extends BasePage {

    @FindBy(id = "email_create")
    WebElement emailField;


    @FindBy(css = ".btn.btn-default.button.button-medium.exclusive")
    WebElement submitButton;

    public SignUpSignIn(WebDriver driver) {
        super(driver);
    }

    public void enterEmailId(String emailId) {
        waitForVisibilityOf(emailField);
        emailField.sendKeys(emailId);
    }

    public void clickCreateAnAccount() {
        waitForElementToBeClickable(submitButton);
        submitButton.submit();
    }
}
