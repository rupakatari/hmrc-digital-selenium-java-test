package gov.hmrc;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features ="src/main/resources/features",
        glue={"gov.hmrc.bdd"},
        format ={"html:target/reports"}
)

public class TestRunAll {

    public TestRunAll() {
    }
}